Fancyload
=====================

This lightweight module will automatically provide lazyloading of images on your website in a pinterest-style color scheme. It fetches the main color of your image and serves it until your image is loaded.

### Usage

* Download and install the reponsive lazy loader library. Link: https://github.com/jetmartin/responsive-lazy-loader
* Place the folder inside the libraries folder so that jquery.responsivelazyloader.js and jquery.responsivelazyloader.min.js are found in libraries/responsive-lazy-loader/js
* Enable the module
* Clear the cache of your website
