/**
 * @file
 * Provides lazy load behaviours.
 */

( function ($) {
  Drupal.behaviors.fancyload = {
    attach: function(context,settings) {
      $(".fancyload").once().responsivelazyloader({
        distance: -100,
      });
    }
  };
})(jQuery);
